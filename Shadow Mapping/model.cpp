#include "model.hpp"
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

Model::Model()
{

}

Model::~Model()
{

}

bool Model::load(const char path[])
{
	Assimp::Importer importer;

	const aiScene *scene = importer.ReadFile(path,
		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_GenUVCoords |
		aiProcess_SortByPType |
		aiProcess_FlipUVs
	);

	if(scene)
	{
		load(scene);
	}
	else
	{
		std::cerr << "Failed to load " << path << std::endl;
	}

	return scene != nullptr;
}

bool Model::load(const aiScene *scene)
{
	meshes.resize(scene->mNumMeshes);
	/*
	const aiMatrix4x4 &t = scene->mRootNode->mTransformation;

	printf(
		"%2f, %2f, %2f, %2f\n"
		"%2f, %2f, %2f, %2f\n"
		"%2f, %2f, %2f, %2f\n"
		"%2f, %2f, %2f, %2f\n\n",
		t.a1, t.a2, t.a3, t.a4,
		t.b1, t.b2, t.b3, t.b4,
		t.c1, t.c2, t.c3, t.c4,
		t.d1, t.d2, t.d3, t.d4);
	*/
	for(unsigned int i = 0; i < scene->mNumMeshes; i++)
	{
		meshes[i].load(scene->mMeshes[i]);
	}

	return true;
}