#ifndef NOISE_HPP
#define NOISE_HPP

#include <random>
#include <GL/glew.h>

class Noise
{
	friend class Terrain;
public:
	Noise();
	~Noise();
	void allocate(size_t w, size_t h);
	void generate(std::mt19937 &engine, size_t w, size_t h, GLfloat amplitude);
	void generate(std::mt19937 &engine, size_t w, size_t h, GLfloat amplitude, size_t smoothness);
	void smooth();
private:
	GLfloat *data;
	size_t w;
	size_t h;
};

#endif