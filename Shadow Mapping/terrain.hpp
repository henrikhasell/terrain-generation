#ifndef TERRAIN_HPP
#define TERRAIN_HPP

#include <random>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include "mesh.hpp"

class Terrain
{
public:
	static constexpr float Amplitude = 5.0f;
	static constexpr float TileW = 1.0f;
	static constexpr float TileH = 1.0f;

	Mesh mesh;

	Terrain();
	~Terrain();

	GLfloat height(size_t x, size_t z);

	void allocate(size_t width, size_t height);
	void createFlat(size_t width, size_t height);
	void createNoise(size_t width, size_t height);
	void createSmoothNoise(size_t width, size_t height);
	void createInterpolatedNoise(size_t width, size_t height);
	void generateTexture();
	void generateMesh();
	bool load(SDL_Surface *surface);
	bool load(const char path[]);
	void bind(GLenum texture = GL_TEXTURE0) const;
private:
	std::mt19937 mt;

	size_t w;
	size_t h;
	GLfloat *heights;
	GLuint texture;
};

#endif