#include "noise.hpp"
#include <SDL2/SDL_stdinc.h> // M_PI

static inline float slerp(GLfloat a, GLfloat b, GLfloat f)
{
	float factor = (float)(0.5 - cos(M_PI * (double)f) * 0.5);
	return a * (1.0f - factor) + b * factor;
}

Noise::Noise() :
	data(nullptr),
	w(0),
	h(0)
{

}

Noise::~Noise()
{
	if(data)
	{
		delete[] data;
	}
}

void Noise::allocate(size_t w, size_t h)
{
	this->w = w;
	this->h = h;

	if(data)
	{
		delete[] data;
	}

	data = new GLfloat[w * h];
}

void Noise::generate(std::mt19937 &engine, size_t w, size_t h, GLfloat amplitude)
{
	allocate(w, h);

	std::uniform_real_distribution<float> distribution(-amplitude, amplitude);

	for(size_t i = 0; i < w * h; i++)
	{
		data[i] = distribution(engine);
	}
}

void Noise::generate(std::mt19937 &engine, size_t w, size_t h, GLfloat amplitude, size_t smoothness)
{
	allocate(w, h);

	size_t w_points = 2 + w / smoothness;
	size_t h_points = 2 + h / smoothness;

	Noise points;
	points.generate(engine, w_points, h_points, amplitude);

	for(size_t x = 0; x < w; x++)
	{
		for(size_t z = 0; z < h; z++)
		{
			const size_t lower_x = x / smoothness;
			const size_t lower_z = z / smoothness;
			const GLfloat prog_x = (GLfloat)(x % smoothness) / (GLfloat)smoothness;
			const GLfloat prog_z = (GLfloat)(z % smoothness) / (GLfloat)smoothness;
			const GLfloat v1 = points.data[lower_x + w_points * (lower_z + 0) + 0];
			const GLfloat v2 = points.data[lower_x + w_points * (lower_z + 0) + 1];
			const GLfloat v3 = points.data[lower_x + w_points * (lower_z + 1) + 0];
			const GLfloat v4 = points.data[lower_x + w_points * (lower_z + 1) + 1];
			const GLfloat i1 = slerp(v1, v2, prog_x);
			const GLfloat i2 = slerp(v3, v4, prog_x);

			data[x + w * z] = slerp(i1, i2, prog_z);
		}
	}
}

void Noise::smooth()
{
	constexpr float corn_cost = 0.4f / 4.6f;
	constexpr float side_cost = 0.5f / 4.6f;
	constexpr float curr_cost = 0.5f;

	GLfloat *smoothData = new GLfloat[w * h];

	for(size_t x = 0; x < w; x++)
	{
		for(size_t z = 0; z < h; z++)
		{
			GLfloat &selected = smoothData[x + w * z];
			selected = 0.0f;

			selected += data[x + (w * z + 0) + 0] * curr_cost;

			if(x > 0)
			{
				selected += data[x + (w * z + 0) - 1] * side_cost;

				if(z > 0)
				{
					selected += data[x + (w * z - 1) - 1] * corn_cost;
				}

				if(z < h - 1)
				{
					selected += data[x + (w * z + 1) - 1] * corn_cost;
				}
			}
			if(x < w - 1)
			{
				selected += data[x + (w * z + 0) + 1] * side_cost;

				if(z > 0)
				{
					selected += data[x + (w * z - 1) + 1] * corn_cost;
				}

				if(z < h - 1)
				{
					selected += data[x + (w * z + 1) + 1] * corn_cost;
				}
			}
			if(z > 0)
			{
				selected += data[x + (w * z - 1) + 0] * side_cost;
			}
			if(z < h - 1)
			{
				selected += data[x + (w * z + 1) + 0] * side_cost;
			}
		}
	}

	delete[] data;

	data = smoothData;
}