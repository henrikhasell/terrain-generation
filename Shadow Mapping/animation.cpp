#include "animation.hpp"
#include <queue>
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <SDL2/SDL.h>

static void buildNodeTree(
	std::list<Node> &nodes,
	std::map<std::string, Node*> &nodeMap,
	Node &current, aiNode *data)
{
	for(unsigned int i = 0; i < data->mNumChildren; i++)
	{
		nodes.emplace_back(&current);
		Node &childNode = nodes.back();
		buildNodeTree(nodes, nodeMap, childNode, data->mChildren[i]);
	}

	nodeMap[data->mName.data] = &current;
}

glm::quat Channel::rotation(double time) const
{
	size_t i;

	time = fmod(time, rotationKeys.back().time);

	for(i = 0; rotationKeys[i + 1].time <= time; i++);

	const QuatKey *curr = &rotationKeys[i + 0];
	const QuatKey *next = &rotationKeys[i + 1];

	double progress = (time - curr->time) / (next->time - curr->time);

	return glm::slerp(curr->value, next->value, (float)progress);
}

glm::vec3 Channel::position(double time) const
{
	size_t i;

	time = fmod(time, positionKeys.back().time);

	for(i = 0; positionKeys[i + 1].time <= time; i++);

	const Vec3Key *curr = &positionKeys[i + 0];
	const Vec3Key *next = &positionKeys[i + 1];

	const double progress = (time - curr->time) / (next->time - curr->time);

	return glm::mix(curr->value, next->value, progress);
}

glm::vec3 Channel::scaling(double time) const
{
	size_t i;

	time = fmod(time, scalingKeys.back().time);

	for(i = 0; scalingKeys[i + 1].time <= time; i++);

	const Vec3Key *curr = &scalingKeys[i + 0];
	const Vec3Key *next = &scalingKeys[i + 1];

	const double progress = (time - curr->time) / (next->time - curr->time);

	return glm::mix(curr->value, next->value, progress);
}

glm::mat4  Node::transform(size_t index, double time) const
{
	const Channel &channel = channels[index];

	glm::vec3 position;
	glm::quat rotation;

	if(!channel.positionKeys.empty())
		position = channel.position(time);
	if(!channel.rotationKeys.empty())
		rotation = channel.rotation(time);

	const glm::mat4 matrix = glm::translate(glm::mat4(1.0f), position) * glm::toMat4(rotation);

	return parent ? parent->transform(index, time) * matrix : matrix;
}

AnimatedMesh::AnimatedMesh() :
	positionArray(nullptr),
	normalArray(nullptr),
	v_count(0),
	i_count(0)
{
	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &normalBuffer);
	glGenBuffers(1, &texCoordBuffer);
	glGenBuffers(1, &indexBuffer);
}


AnimatedMesh::~AnimatedMesh()
{
	if(positionArray)
	{
		delete[] positionArray;
	}
	if(normalArray)
	{
		delete[] normalArray;
	}
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &normalBuffer);
	glDeleteBuffers(1, &texCoordBuffer);
	glDeleteBuffers(1, &indexBuffer);
}

bool AnimatedMesh::load(
	const aiMesh *mesh,
	const std::map<std::string, Node*> &map)
{
	if(positionArray)
	{
		return false;
	}

	positionArray = new GLfloat[mesh->mNumVertices * 3];
	normalArray = new GLfloat[mesh->mNumVertices * 3];

	GLfloat *textureCoordArray = new GLfloat[mesh->mNumVertices * 2];
	GLuint *indexArray = new GLuint[mesh->mNumFaces * 3];

	vertices.resize(mesh->mNumVertices);

	for(unsigned int i = 0; i < mesh->mNumVertices; i++)
	{
		const aiVector3D *vertex = mesh->mVertices + i;
		const aiVector3D *normal = mesh->mNormals + i;
		const aiVector3D *textureCoord = *mesh->mTextureCoords + i;

		positionArray[i * 3 + 0] = vertex->x;
		positionArray[i * 3 + 1] = vertex->y;
		positionArray[i * 3 + 2] = vertex->z;

		normalArray[i * 3 + 0] = normal->x;
		normalArray[i * 3 + 1] = normal->y;
		normalArray[i * 3 + 2] = normal->z;

		if(*mesh->mTextureCoords)
		{
			const aiVector3D *textureCoord = *mesh->mTextureCoords + i;

			textureCoordArray[i * 2 + 0] = textureCoord->x;
			textureCoordArray[i * 2 + 1] = textureCoord->y;
		}
		else
		{
			textureCoordArray[i * 2 + 0] = 0.0f;
			textureCoordArray[i * 2 + 1] = 0.0f;
		}
	}

	for(unsigned int i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace *face = mesh->mFaces + i;

		for(unsigned int j = 0; j < face->mNumIndices; j++)
		{
			indexArray[i * 3 + j] = face->mIndices[j];
		}
	}

	bones.resize(mesh->mNumBones);

	for(unsigned int i = 0; i < mesh->mNumBones; i++)
	{
		const aiBone *currentBone = mesh->mBones[i];
		auto iterator = map.find(currentBone->mName.data);

		if(iterator == map.end())
		{
			std::cerr << "Bone does not have matching node." << std::endl;
			return false;
		}

		Bone &bone = bones[i];
		bone.offset[0][0] = currentBone->mOffsetMatrix.a1;
		bone.offset[0][1] = currentBone->mOffsetMatrix.b1;
		bone.offset[0][2] = currentBone->mOffsetMatrix.c1;
		bone.offset[0][3] = currentBone->mOffsetMatrix.d1;
		bone.offset[1][0] = currentBone->mOffsetMatrix.a2;
		bone.offset[1][1] = currentBone->mOffsetMatrix.b2;
		bone.offset[1][2] = currentBone->mOffsetMatrix.c2;
		bone.offset[1][3] = currentBone->mOffsetMatrix.d2;
		bone.offset[2][0] = currentBone->mOffsetMatrix.a3;
		bone.offset[2][1] = currentBone->mOffsetMatrix.b3;
		bone.offset[2][2] = currentBone->mOffsetMatrix.c3;
		bone.offset[2][3] = currentBone->mOffsetMatrix.d3;
		bone.offset[3][0] = currentBone->mOffsetMatrix.a4;
		bone.offset[3][1] = currentBone->mOffsetMatrix.b4;
		bone.offset[3][2] = currentBone->mOffsetMatrix.c4;
		bone.offset[3][3] = currentBone->mOffsetMatrix.d4;
		bones[i].node = iterator->second;

		for(unsigned int j = 0; j < currentBone->mNumWeights; j++)
		{
			aiVertexWeight *currentWeight = currentBone->mWeights + j;

			vertices[currentWeight->mVertexId].boneIndices.push_back(i);
			vertices[currentWeight->mVertexId].boneWeights.push_back(currentWeight->mWeight);
		}

	}

	v_count = mesh->mNumVertices;
	i_count = mesh->mNumFaces * 3;

	glBindBuffer(GL_ARRAY_BUFFER, texCoordBuffer);
	glBufferData(GL_ARRAY_BUFFER, v_count * sizeof(GLfloat) * 2, textureCoordArray, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ARRAY_BUFFER, i_count * sizeof(GLuint), indexArray, GL_STATIC_DRAW);

	delete[] textureCoordArray;
	delete[] indexArray;

	return true;
}

void AnimatedMesh::transform(double time) const
{
	std::vector<glm::mat4> matrixArray(bones.size());

	for(size_t i = 0; i < matrixArray.size(); i++)
	{
		matrixArray[i] = bones[i].node->transform(0, time) * bones[i].offset;
	}

	size_t bufferSize = v_count * sizeof(GLfloat) * 3;

	GLfloat *positionArray2 = new GLfloat[bufferSize];
	GLfloat *normalArray2 = new GLfloat[bufferSize];

	for(unsigned int j = 0; j < vertices.size(); j++)
	{
		glm::mat4 transformation(0.0f);

		const AnimationVertex &vertex = vertices[j];

		for(unsigned int i = 0; i < vertex.boneIndices.size(); i++)
		{
			GLuint boneIndex = vertex.boneIndices[i];
			GLfloat boneWeight = vertex.boneWeights[i];

			transformation += matrixArray[boneIndex] * boneWeight;
		}
		glm::vec3 &position = *(glm::vec3*)(positionArray + j * 3);
		glm::vec3 &normal = *(glm::vec3*)(normalArray + j * 3);

		glm::vec3 &position2 = *(glm::vec3*)(positionArray2 + j * 3);
		glm::vec3 &normal2 = *(glm::vec3*)(normalArray2 + j * 3);

		position2 = transformation  * glm::vec4(position, 1.0f);
		normal2 = glm::normalize(glm::mat3(transformation)  * normal);
	}

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, positionArray2, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, normalArray2, GL_DYNAMIC_DRAW);

	delete[] positionArray2;
	delete[] normalArray2;
}

AnimatedModel::AnimatedModel()
{
}

AnimatedModel::~AnimatedModel()
{
}

bool AnimatedModel::load(const char path[])
{
	Assimp::Importer importer;

	const aiScene *scene = importer.ReadFile(path,
		aiProcess_GenSmoothNormals |
		aiProcess_Triangulate |
		aiProcess_GenUVCoords |
		aiProcess_SortByPType |
		aiProcess_FlipUVs);

	if(scene)
	{
		load(scene);
	}
	else
	{
		std::cerr << "Failed to load " << path << std::endl;
	}

	return scene != nullptr;
}

bool AnimatedModel::load(const aiScene * scene)
{
	std::map<std::string, Node*> nodeMap;

	nodeList.emplace_back(nullptr);
	buildNodeTree(nodeList, nodeMap, nodeList.back(), scene->mRootNode);

	for(Node &node : nodeList)
	{
		node.channels.resize(scene->mNumAnimations);
	}

	meshes.resize(scene->mNumMeshes);

	for(unsigned int i = 0; i < scene->mNumMeshes; i++)
	{
		const aiMesh *mesh = scene->mMeshes[i];
		meshes[i].load(mesh, nodeMap);
	}

	for(unsigned int i = 0; i < scene->mNumAnimations; i++)
	{
		load(scene->mAnimations[i], nodeMap, i);
	}

	return true;
}

bool AnimatedModel::load(
	const aiAnimation *animation,
	const std::map<std::string, Node*> &map,
	unsigned int index)
{
	for(unsigned int i = 0; i < animation->mNumChannels; i++)
	{
		const aiNodeAnim *nodeAnimation = animation->mChannels[i];
		auto iterator = map.find(nodeAnimation->mNodeName.data);

		if(iterator == map.end())
		{
			std::cerr << "Channel does not have matching node." << std::endl;
			return false;
		}

		Node *node = iterator->second;
		Channel &channel = node->channels[index];
		double ticksPerSecond = animation->mTicksPerSecond != 0.0
			? animation->mTicksPerSecond : 1.0;

		channel.rotationKeys.resize(nodeAnimation->mNumRotationKeys);
		channel.positionKeys.resize(nodeAnimation->mNumPositionKeys);
		channel.scalingKeys.resize(nodeAnimation->mNumScalingKeys);

		for(unsigned int j = 0; j < nodeAnimation->mNumRotationKeys; j++)
		{
			channel.rotationKeys[j].time = nodeAnimation->mRotationKeys[j].mTime / ticksPerSecond;
			channel.rotationKeys[j].value.w = nodeAnimation->mRotationKeys[j].mValue.w;
			channel.rotationKeys[j].value.x = nodeAnimation->mRotationKeys[j].mValue.x;
			channel.rotationKeys[j].value.y = nodeAnimation->mRotationKeys[j].mValue.y;
			channel.rotationKeys[j].value.z = nodeAnimation->mRotationKeys[j].mValue.z;
		}

		for(unsigned int j = 0; j < nodeAnimation->mNumPositionKeys; j++)
		{
			channel.positionKeys[j].time = nodeAnimation->mPositionKeys[j].mTime / ticksPerSecond;
			channel.positionKeys[j].value.x = nodeAnimation->mPositionKeys[j].mValue.x;
			channel.positionKeys[j].value.y = nodeAnimation->mPositionKeys[j].mValue.y;
			channel.positionKeys[j].value.z = nodeAnimation->mPositionKeys[j].mValue.z;
		}

		for(unsigned int j = 0; j < nodeAnimation->mNumScalingKeys; j++)
		{
			channel.scalingKeys[j].time = nodeAnimation->mScalingKeys[j].mTime / ticksPerSecond;
			channel.scalingKeys[j].value.x = nodeAnimation->mScalingKeys[j].mValue.x;
			channel.scalingKeys[j].value.y = nodeAnimation->mScalingKeys[j].mValue.y;
			channel.scalingKeys[j].value.z = nodeAnimation->mScalingKeys[j].mValue.z;
		}
	}

	return true;
}

void AnimatedModel::transform(double time) const
{
	for(const AnimatedMesh &mesh : meshes)
	{
		mesh.transform(time);
	}
}

AnimatedObject::AnimatedObject() :
	scale(1.0f, 1.0f, 1.0f),
	position(0.0f, 0.0f, 0.0f),
	rotation(1.0f, 0.0f, 0.0f, 0.0f),
	texture(nullptr),
	model(nullptr),
	progress(0.0)
{
}

glm::mat4 AnimatedObject::modelMatrix() const
{
	return glm::translate(glm::mat4(1.0f), position) * glm::toMat4(rotation) * glm::scale(glm::mat4(1.0f), scale);
}
