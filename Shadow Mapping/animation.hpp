#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <map>
#include <list>
#include <vector>
#include <string>
#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <GL/glew.h>
#include "texture.hpp"

template <class T>
struct KeyFrame
{
	double time;
	T value;
};

typedef KeyFrame<glm::quat> QuatKey;
typedef KeyFrame<glm::vec3> Vec3Key;

struct Channel
{
	std::vector<QuatKey> rotationKeys;
	std::vector<Vec3Key> positionKeys;
	std::vector<Vec3Key> scalingKeys;

	glm::quat rotation(double time) const;
	glm::vec3 position(double time) const;
	glm::vec3 scaling(double time) const;
};

struct Node
{
	Node(Node *parent) :
		parent(parent)
	{}

	glm::mat4 transform(size_t index, double time) const;

	std::vector<Channel> channels;
	Node *parent;
};

struct AnimationVertex
{
	std::vector<GLfloat> boneWeights;
	std::vector<GLuint> boneIndices;
};

struct Bone
{
	glm::mat4 offset;
	Node *node;
};

class AnimatedMesh
{
	friend class Renderer;
	friend class ShadowMap;
public:
	AnimatedMesh();
	~AnimatedMesh();
	bool load(
		const aiMesh *mesh,
		const std::map<std::string, Node*> &map);
	void transform(double time) const;
private:
	GLfloat *positionArray;
	GLfloat *normalArray;

	std::vector<AnimationVertex> vertices;
	std::vector<Bone> bones;

	GLuint vertexBuffer;
	GLuint normalBuffer;
	GLuint texCoordBuffer;
	GLuint indexBuffer;

	GLsizei v_count;
	GLsizei i_count;
};

class AnimatedModel
{
	friend class Renderer;
	friend class ShadowMap;
public:
	AnimatedModel();
	~AnimatedModel();
	bool load(const char path[]);
	bool load(const aiScene *scene);
	bool load(
		const aiAnimation *animation,
		const std::map<std::string, Node*> &map,
		unsigned int index);
	void transform(double time) const;
private:
	std::vector<AnimatedMesh> meshes;
	std::list<Node> nodeList;
};

struct AnimatedObject
{
	AnimatedObject();

	void rotate(
		GLfloat x,
		GLfloat y,
		GLfloat z,
		GLfloat amount);

	glm::mat4 modelMatrix() const;

	glm::vec3 scale;
	glm::vec3 position;
	glm::quat rotation;

	Texture *texture;
	AnimatedModel *model;
	double progress;
};

#endif