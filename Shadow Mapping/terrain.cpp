#include "terrain.hpp"
#include "noise.hpp"
#include <iostream>
#include <GL/glew.h>
#include <SDL2/SDL_image.h>
#include <glm/vec3.hpp>
#include <glm/geometric.hpp>

Terrain::Terrain() :
	mt(0),
	w(0),
	h(0),
	heights(nullptr)
{
	glGenTextures(1, &texture);
}

Terrain::~Terrain()
{
	if(heights)
	{
		delete[] heights;
	}

	glDeleteTextures(1, &texture);
}

GLfloat Terrain::height(size_t x, size_t z)
{
	return heights[x + w * z];
}

void Terrain::allocate(size_t width, size_t height)
{
	w = width;
	h = height;

	if(heights)
	{
		delete[] heights;
	}

	heights = new float[width * height];
}

void Terrain::createFlat(size_t width, size_t height)
{
	allocate(width, height);

	for(size_t i = 0; i < width * height; i++)
	{
		heights[i] = 0.0f;
	}

	generateMesh();
}

void Terrain::createNoise(size_t width, size_t height)
{
	allocate(width, height);

	Noise noise;
	noise.generate(mt, width, height, Amplitude);

	for(size_t i = 0; i < noise.w * noise.h; i++)
	{ // Inefficient copy.
		heights[i] = noise.data[i];
	}

	generateMesh();
}

void Terrain::createSmoothNoise(size_t width, size_t height)
{
	allocate(width, height);

	Noise noise;
	noise.generate(mt, width, height, Amplitude);
	noise.smooth();

	for(size_t i = 0; i < noise.w * noise.h; i++)
	{ // Inefficient copy.
		heights[i] = noise.data[i];
	}

	generateMesh();
}

void Terrain::createInterpolatedNoise(size_t width, size_t height)
{
	allocate(width, height);


	Noise noise;
	noise.generate(mt, width, height, Amplitude, 7);

	for(size_t i = 0; i < noise.w * noise.h; i++)
	{ // Inefficient copy.
		heights[i] = noise.data[i];
	}

	generateMesh();
}

void Terrain::generateTexture()
{
	glBindTexture(GL_TEXTURE_2D, texture);

	GLfloat *normalisedHeights = new GLfloat[w * h];

	for(size_t i = 0; i < w * h; i++)
	{
		normalisedHeights[i] = (heights[i] / Amplitude + 1.0f) / 2.0f;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, 1,
		w,
		h, 0,
		GL_RED, GL_FLOAT,
		normalisedHeights
	);

	delete[] normalisedHeights;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void Terrain::generateMesh()
{
	if(w < 2 || h < 2)
	{
		return;
	}

	const size_t w_1 = w - 1;
	const size_t h_1 = h - 1;

	GLfloat *vertices = new GLfloat[w_1 * h_1 * 4 * 3];
	GLfloat *normals = new GLfloat[h_1 * h_1 * 4 * 3];
	GLfloat *textures = new GLfloat[w_1 * h_1 * 4 * 2];
	GLuint *indices = new GLuint[w_1 * h_1 * 6];

	GLfloat *normalsByCoord = new GLfloat[w * h * 3];

	for(size_t i = 0; i < w * h; i++)
	{
		const size_t x = i / w;
		const size_t z = i % w;

		glm::vec3 normal(0.0f, 0.0f, 0.0f), up, down, left, right;

		if(x > 0)
		{
			left = glm::vec3(-TileW, height(x - 1, z) - height(x, z), 0.0f);
		}
		if(z > 0)
		{
			up = glm::vec3(0.0f, height(x, z - 1) - height(x, z), -TileH);
		}
		if(x < w_1)
		{
			right = glm::vec3(+TileW, height(x + 1, z) - height(x, z), 0.0f);
		}
		if(z < h_1)
		{
			down = glm::vec3(0.0f, height(x, z + 1) - height(x, z), TileH);
		}

		if(x < w_1 && z > 0)
		{
			normal += glm::cross(right, up);
		}

		if(x > 0 && z > 0)
		{
			normal += glm::cross(up, left);
		}

		if(x > 0 && z < h_1)
		{
			normal += glm::cross(left, down);
		}

		if(x < w_1 && z < h_1)
		{
			normal += glm::cross(down, right);
		}

		normal = glm::normalize(normal);

		normalsByCoord[(x + w * z) * 3 + 0] = normal.x;
		normalsByCoord[(x + w * z) * 3 + 1] = normal.y;
		normalsByCoord[(x + w * z) * 3 + 2] = normal.z;
	}

	for(size_t x = 0; x < w_1; x++)
	{
		for(size_t z = 0; z < h_1; z++)
		{
			size_t index = (x + w_1 * z) * 12;

			size_t offsets[4][2] = {
				{0, 0},
				{1, 0},
				{0, 1},
				{1, 1}
			};

			for(size_t i = 0; i < 4; i++)
			{
				size_t &offset_x = offsets[i][0];
				size_t &offset_z = offsets[i][1];

				vertices[index + i * 3 + 0] = TileW * (GLfloat)(x + offset_x) - (w_1 * TileW) / 2.0f;
				vertices[index + i * 3 + 1] = (GLfloat)height(x + offset_x, z + offset_z) - 5.0f;
				vertices[index + i * 3 + 2] = TileH * (GLfloat)(z + offset_z) - (h_1 * TileH) / 2.0f;

				normals[index + i * 3 + 0] = normalsByCoord[(x + offset_x + w * (z + offset_z)) * 3 + 0];
				normals[index + i * 3 + 1] = normalsByCoord[(x + offset_x + w * (z + offset_z)) * 3 + 1];
				normals[index + i * 3 + 2] = normalsByCoord[(x + offset_x + w * (z + offset_z)) * 3 + 2];
			}

		}
	}

	delete[] normalsByCoord;

	for(size_t x = 0; x < w_1; x++)
	{
		for(size_t z = 0; z < h_1; z++)
		{
			size_t index = (x + w_1 * z) * 6;
			GLuint value = (GLuint)(x + w_1 * z) * 4;

			indices[index + 0] = value + 0;
			indices[index + 1] = value + 2;
			indices[index + 2] = value + 1;

			indices[index + 3] = value + 1;
			indices[index + 4] = value + 2;
			indices[index + 5] = value + 3;
		}
	}

	for(size_t i = 0; i < w_1 * h_1; i++)
	{
		textures[i * 8 + 0] = 0.0f;
		textures[i * 8 + 1] = 0.0f;

		textures[i * 8 + 2] = 1.0f;
		textures[i * 8 + 3] = 0.0f;

		textures[i * 8 + 4] = 0.0f;
		textures[i * 8 + 5] = 1.0f;

		textures[i * 8 + 6] = 1.0f;
		textures[i * 8 + 7] = 1.0f;
	}

	mesh.load(
		vertices,
		normals,
		textures,
		indices,
		(GLuint)(w_1 * h_1) * 4,
		(GLuint)(w_1 * h_1) * 6);

	generateTexture();

	delete[] vertices;
	delete[] normals;
	delete[] textures;
	delete[] indices;
}

static inline Uint8 *getPixel(SDL_Surface *surface, int x, int y)
{
	return (Uint8*)surface->pixels + (surface->pitch * y) + (x * surface->format->BytesPerPixel);
}

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
#   define R_MASK 0xFF000000
#   define G_MASK 0x00FF0000
#   define B_MASK 0x0000FF00
#   define A_MASK 0x000000FF
#else
#   define R_MASK 0x000000FF
#   define G_MASK 0x0000FF00
#   define B_MASK 0x00FF0000
#   define A_MASK 0xFF000000
#endif

bool Terrain::load(SDL_Surface *surface)
{
	SDL_Surface *optimised = SDL_CreateRGBSurface(0, surface->w, surface->h, 32, R_MASK, G_MASK, B_MASK, A_MASK);

	if(optimised)
	{
		SDL_FillRect(optimised, NULL, 0x000000ff);
		SDL_SetSurfaceAlphaMod(surface, 0xFF);
		SDL_SetSurfaceBlendMode(surface, SDL_BLENDMODE_NONE);

		SDL_BlitSurface(surface, NULL, optimised, NULL);

		createNoise(optimised->w, optimised->h);
		
		for(int x = 0; x < optimised->w; x++)
		{
			for(int z = 0; z < optimised->h; z++)
			{
				Uint8 *pixel = getPixel(optimised, x, z);
				heights[x + w * z] = ((float)*pixel / 256.0f);
			}
		}

		generateMesh();

		SDL_FreeSurface(optimised);
	}

	return optimised != nullptr;
}

#undef R_MASK
#undef G_MASK
#undef B_MASK
#undef A_MASK

bool Terrain::load(const char path[])
{
	bool success = false;
	SDL_Surface *surface = IMG_Load(path);

	if(surface)
	{
		success = load(surface);
		SDL_FreeSurface(surface);
	}

	return success;
}

void Terrain::bind(GLenum texture) const
{
	glActiveTexture(texture);
	glBindTexture(GL_TEXTURE_2D, this->texture);
}